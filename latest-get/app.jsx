var http = require('http');
var fs = require('fs');
var AWS = require('aws-sdk');

// Parameters
var BUCKET_DEST = 'demiurgeosmbucket';
var LATEST_URL = 'http://planet.osm.org/replication/hour/state.txt';
var CHECKPOINT_LIMIT = 50;
var DOWNLOAD_LIMIT = 3;
var PLANET_URL = 'http://planet.openstreetmap.org/';

var s3 = new AWS.S3();

/* This function pads integer with zeroes, ex 1 -> '001' */
function zeroPad(num, length) {
    var r = "" + num;
    if (!length){
        length = 3;
    }
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
}

/*  this downloads a file and passes it callback
    TODO: replace with request
*/
function  getFile(url, finished){
    // this should be replaced by request library
    http.get(url, function(response) {
        var body = '';
        response.on('data', function (chunk) {
            body += chunk;
        });
        response.on('end', function () {
            finished(body);
        });
    }).on('error', function(e) {
        console.error("Error:", e);
    });
}


class ReplicationChecker {
    constructor(context){
        this.context = context;
    }

    /*
        This parses the state file and synchronizes up to latest sequence.
        input: text
    */
    updateToState(text){
        var lines = text.split('\n');
        var sequence_tokens = lines[1].split('=');
        var latest_sequence = sequence_tokens[1];

        this.syncSequenceToBucket(latest_sequence);
    }

    /*
        Takes sequence string and queries the bucket for file inventory
        passes on sequence and inventory to a function that handles download
        input: sequence
        TODO: it doesn't do anything with sequence, refactor not to take it
    */
    syncSequenceToBucket(sequence){
        var self = this;

        var params = {
            'Bucket': BUCKET_DEST,
        };

        s3.listObjectsV2(params, (err, data) => {
            if (err) {
                console.error(err, err.stack);
            } else {
                var present_keys = data.Contents.map((item) => item.Key);
                self.syncKeys(present_keys, sequence);
            }
        });

    }

    /*
        Finds the difference between inventory keys and downloadable keys
        inputs: string array of keys, latest sequence string
        TODO: currently only iterates over file sequence, iterate over dirs
    */
    syncKeys(present_keys, sequence){
        var self = this;

        var state_dir_seq = parseInt(sequence.slice(0, -3));
        var state_file_seq = parseInt(sequence.slice(2));

        self.keys_to_get = [];
        // Only add up to the limit of files to get in one lambda
        for(
            var check_count=0;
            (
                (check_count < CHECKPOINT_LIMIT) &&
                (self.keys_to_get.length < DOWNLOAD_LIMIT)
            );
            check_count++
        ){
            var dir_seq = state_dir_seq;
            var file_seq = state_file_seq - check_count;

            var file_key = (
                'replication/hour/000/' +
                zeroPad(dir_seq) +
                '/' +
                zeroPad(file_seq) +
                '.osc.gz'
            );

            if (present_keys.indexOf(file_key) == -1){
                self.keys_to_get.push(file_key);
            }
        }

        console.log('Downloading:', self.keys_to_get);

        if(self.keys_to_get){
            self.keys_to_get.map((key) => self.getReplicationFile(key));
        } else {
            self.context.done(null, 'success, no downloads');
        }
    }

    /*
        Uses a path to replication file to download it and put it in bucket
        input: path exactly like website, replication/hour/000/000/000.osc.gz
    */
    getReplicationFile(file_key){
        var self = this;

        var file_url = PLANET_URL + file_key
        console.log("Downloading:", file_url);

        getFile(file_url, (data) => {
            var params = {
                'Bucket': BUCKET_DEST,
                'Key': file_key,
                'Body': data
            };
            s3.putObject(params, (err, data) => {
                if (err) {
                    console.error(err, err.stack);
                } else {
                    if (self.keys_to_get.length == 0){
                        console.log("Downloaded", file_key);
                        self.context.done(null, 'success, downloaded');
                    }
                }
            });
        });
    }

    /*
        Main entry point and execution of synchronization of replication
        files to an S3 Bucket.
    */
    run() {
        getFile(LATEST_URL, (text) => this.updateToState(text));
    }
}

exports.handler = function(event, context) {
    var checker = new ReplicationChecker(context);
    checker.run();
};
