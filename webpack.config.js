const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');

const debug = process.env.NODE_ENV !== 'production';

module.exports = {
  context: __dirname,
  entry: './latest-get/app.jsx',
  output: {
    path: __dirname,
    filename: './latest-get/index.js',
    library: "index",
    libraryTarget: 'commonjs2'
  },
  module: {
    rules: [
      {
        test: /\.jsx$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      }
    ],
  },
  target: 'node',
  externals: [ nodeExternals(), "aws-sdk"],
  optimization: {
    minimize: false
  },
  mode: 'production'
};