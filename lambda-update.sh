#!/usr/bin/env bash

export FNAME='latest-get'

zip -j out.zip "./$FNAME/index.js"

aws lambda update-function-code \
--function-name $FNAME  \
--zip-file fileb://out.zip \
--profile teatuser

rm out.zip