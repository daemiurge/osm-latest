This project implements a lambda function which updates an S3 bucket with the
latest hourly OSM replication data.

*Building*

    npm install
    npm run build

*Deploying*

    S3 role credentials must be configured with valid permissions in script
        lambda-create.sh
    If the role, and user permissions are correct
        ./lambda-create.sh
    will create latest-get lambda function for the configured role.

*Functionality*

1. checks the latest available OSM replication data
2. checks the Amazon S3 bucket for existing data
3. find the difference as list of files that can be donwloaded
4. map/recurce the list to separate download calls that create objects

TODO: convert to promises, try streaming files, es6 string templating