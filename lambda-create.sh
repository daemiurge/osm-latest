#!/usr/bin/env bash

export FNAME='latest-get'

zip -j out.zip "./$FNAME/index.js"

aws lambda delete-function --function-name $FNAME --profile teatuser

aws lambda create-function \
--region us-east-1 \
--function-name $FNAME  \
--zip-file fileb://out.zip \
--role arn:aws:iam::292968936464:role/service-role/testrole \
--handler index.handler \
--runtime nodejs6.10 \
--profile teatuser \
--timeout 300 \
--memory-size 512

rm out.zip